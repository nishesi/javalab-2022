insert into client(first_name, last_name, phone_number, age) values ('Nurislam', 'Zaripov', '88005553535', 19);
insert into client(first_name, last_name, phone_number, age) values ('Malik', 'Zaripov', '34634161345341', 14);
insert into client(first_name, last_name, phone_number, age) values ('Azamat', 'Zaripov', '8816346346563', 14);
insert into client(first_name, last_name, phone_number, age) values ( 'Albert', 'Zaripov', '8800362527554', 19);
insert into client(first_name, last_name, phone_number, age) values ( 'Elza', 'Zaripov', '880026235665635', 19);

insert into driver(first_name, last_name, age, driving_experience) values ('Muhamad', 'Imamov', 25, 4);
insert into driver(first_name, last_name, age, driving_experience) values ('Alisher', 'Abdulloev', 30, 7);
insert into driver(first_name, last_name, age, driving_experience) values ('Dilshor', 'Abbosov', 21, 3);

insert into car(last_tech_service, brand, model, colour, license_plate) values ('2021-12-22', 'Lada', 'priora', 'eggplant', 'a123bc');
insert into car(last_tech_service, brand, model, colour, license_plate) values ('2021-06-20', 'Lada', 'kalina', 'white', 'a456gh');
insert into car(last_tech_service, brand, model, colour, license_plate) values ('2021-08-15', 'Kia', 'rio', 'black', 't948dc');

insert into booking(client_id, driver_id, car_id, start_date, end_date) values (1, 1, 1, '2021-12-31 12:30:15', '2021-12-31 12:40:15');
insert into booking(client_id, driver_id, car_id, start_date, end_date) values (1, 2, 3, '2021-12-30 12:30:15', '2021-12-31 12:40:15');
insert into booking(client_id, driver_id, car_id, start_date, end_date) values (1, 2, 3, '2021-12-29 12:30:15', '2021-12-31 12:40:15');

update client set last_name = 'Zaripova', age = 42 where id = 5;
update client set age = 42 where id = 4;
