drop table if exists client;
drop table if exists driver;
drop table if exists car;
drop table if exists booking;

create table client (
    id bigserial primary key,
    first_name char(20),
    last_name char(20),
    phone_number char(20) not null
);

create table driver (
    id bigserial primary key,
    first_name char(20),
    last_name char(20),
    age integer check (age > 18),
    driving_experience integer check (age > 3)

);

create table car (
    id bigserial primary key,
    last_tech_service date,
    brand char(20),
    model char(40),
    colour char(20),
    license_plate char(20) not null
);

create table booking (
    id bigserial primary key,
    client_id bigserial,
    driver_id bigserial,
    car_id bigserial,
    start_date timestamp not null,
    end_date timestamp not null
);

alter table client add age integer check (age >= 14);
alter table booking add foreign key (client_id) references client(id);
alter table booking add foreign key (driver_id) references driver(id);
alter table booking add foreign key (car_id) references car(id);


