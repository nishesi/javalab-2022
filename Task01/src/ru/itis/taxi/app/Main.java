package ru.itis.taxi.app;

import ru.itis.taxi.dto.SignUpForm;
import ru.itis.taxi.mappers.Mappers;
import ru.itis.taxi.models.User;
import ru.itis.taxi.repositories.UsersRepository;
import ru.itis.taxi.repositories.UsersRepositoryFilesImpl;
import ru.itis.taxi.services.UsersService;
import ru.itis.taxi.services.UsersServiceImpl;

import java.util.List;
import java.util.UUID;

public class Main {

    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFilesImpl("users.txt");
        UsersService usersService = new UsersServiceImpl(usersRepository, Mappers::fromSignUpForm);

        UUID testId = UUID.randomUUID();

        User[] users = new User[]{
                new User(UUID.randomUUID(), "Марсель", "Сидиков",
                        "sidikov.marsel@gmail.com", "qwerty007"),

                new User(testId, "Нурислам", "Зарипов",
                        "asdfsadf@gmail.com", "aslkdfsld"),

                new User(UUID.randomUUID(), "Bob", "Зарипов",
                        "asddf@gmail.com", "aslsld"),

                new User(UUID.randomUUID(), "Martin", "Зарипов",
                        "asdsfgadfgfsadf@gmail.com", "aslkdfd")

        };

        for (User user : users) {
            usersRepository.save(user);
        }

        List<User> userList = usersRepository.findAll();

        userList.forEach(System.out::println);
        System.out.println();

        System.out.println(usersRepository.findById(testId).orElse(new User("user not found", "", "", "")));
        System.out.println();

        usersRepository.delete(users[0]);
        usersRepository.findAll().forEach(System.out::println);
        System.out.println();

        usersRepository.update(new User(testId, "new", "updated", "user", "pass"));
        usersRepository.findAll().forEach(System.out::println);
    }
}
