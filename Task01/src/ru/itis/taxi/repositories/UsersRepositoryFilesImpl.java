package ru.itis.taxi.repositories;

import ru.itis.taxi.models.User;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 30.06.2022
 * 02. TaxiService
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class UsersRepositoryFilesImpl implements UsersRepository {

    private final String fileName;
    private long counter;

    private static final String SEPARATOR = ",";
    private static final Function<User, String> userToString = user ->
            user.getId()
                    + SEPARATOR + user.getFirstName()
                    + SEPARATOR + user.getLastName()
                    + SEPARATOR + user.getEmail()
                    + SEPARATOR + user.getPassword();

    private static final Function<String[], User> StringToUser = strArr ->
            new User(UUID.fromString(strArr[0]), strArr[1], strArr[2], strArr[3], strArr[4]);

    public UsersRepositoryFilesImpl(String fileName) {
        this.fileName = fileName;
        this.counter = 0;
    }

    @Override
    public List<User> findAll() {
        try (BufferedReader in = new BufferedReader(new FileReader(fileName))){
            Stream<String> entitiesStream = in.lines();

            return entitiesStream.map(str -> str.split(SEPARATOR)).map(StringToUser).collect(Collectors.toList());
        } catch (IOException ignore) {
            throw new IllegalArgumentException(ignore);
        }
    }

    @Override
    public void save(User entity) {
        counter++;
        try (Writer writer = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(writer)) {

            String userAsString = userToString.apply(entity);
            bufferedWriter.write(userAsString);
            bufferedWriter.newLine();
            writer.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(User entity) {
        List<User> userList = findAll();

        for (User user : userList) {
            if (isEqualId(user.getId(), entity.getId())) {
                userList.remove(user);
                break;
            }
        }
        userList.add(entity);
        saveAll(userList, false);
    }

    @Override
    public void delete(User entity) {
        deleteById(entity.getId());
    }

    @Override
    public void deleteById(UUID id) {
        List<User> updatedUserList = findAll().stream().
                filter(user -> !isEqualId(user.getId(), id)).
                collect(Collectors.toList());
        saveAll(updatedUserList, false);
    }

    @Override
    public Optional<User> findById(UUID id) {
        List<User> users = findAll();

        return users.stream().filter(user -> isEqualId(user.getId(), id)).findFirst();
    }

    private static boolean isEqualId(UUID id1, UUID id2) {
        return id1.toString().equals(id2.toString());
    }

    private void saveAll(List<User> userList, boolean append) {
        try (BufferedWriter out = new BufferedWriter(new FileWriter(fileName, append))) {

            for (User user : userList) {
                String strRepresentation = userToString.apply(user);
                out.write(strRepresentation);
                out.newLine();
            }
        } catch (IOException ignore) {
            throw new IllegalArgumentException(ignore);
        }
    }
}
