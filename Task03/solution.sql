create table Product
(
    maker varchar(10),
    model varchar(50),
    type  varchar(50)
);

create table PC
(
    code  int,
    model varchar(50),
    speed smallint,
    ram   smallint,
    hd    real,
    cd    varchar(10),
    price money,

    foreign key (model) references Product (model)
);

create table Laptop
(
    code   int,
    model  varchar(50),
    speed  smallint,
    ram    smallint,
    hd     real,
    screen smallint,
    price  money,

    foreign key (model) references Product (model)
);

create table Printer
(
    code  int,
    model varchar(50),
    color char(1),
    type  varchar(10),
    price money,

    foreign key (model) references Product (model)
);

create table Classes
(
    class        varchar(50),
    type         varchar(2),
    country      varchar(20),
    numGuns      smallint,
    bore         real,
    displacement int
);

create table Ships
(
    name     varchar(50),
    class    varchar(50),
    launched smallint,

    foreign key (class) references Classes (class)
);

create table Battles
(
    name   varchar(20),
    "date" date
);

create table Outcomes
(
    ship   varchar(50),
    battle varchar(20),
    result varchar(10),

    foreign key (battle) references Battles (name)
);


--1
select model, speed, hd
from PC
where price < 500;

--2
select distinct maker
from Product
where type = 'Printer';

--3
Select model, ram, screen
from Laptop
where price > 1000;

--4
Select *
from Printer
where color = 'y';

--5
Select model, speed, hd
from PC
where (cd = '12x' or cd = '24x')
  and (price < 600);

--6
select distinct Product.maker, Laptop.speed
from Product
         inner join Laptop on Product.model = Laptop.model and Laptop.hd >= 10;

--7
Select distinct PC.model, price
from PC
         inner join Product on PC.model = Product.model and maker = 'B'
union all
Select distinct Laptop.model, price
from Laptop
         inner join Product on Laptop.model = Product.model and maker = 'B'
union all
Select distinct Printer.model, price
from Printer
         inner join Product on Printer.model = Product.model and maker = 'B';

--8
select distinct maker
from Product
where type = 'PC'
except
select distinct maker
from Product
where type = 'Laptop';

--9
select distinct maker
from Product
         inner join PC on Product.model = PC.model and speed >= 450;

--10
select model, price
from Printer
where price = (select max(price) from Printer);

--11
select avg(speed)
from PC;

--12
select avg(speed)
from (select speed from Laptop where price > 1000) as Ls;

--13
select avg(speed)
from (select speed
      from PC
               inner join Product on PC.model = Product.model and maker = 'A') as APClist;

--14
select Ships.class, name, country
from Ships
         inner join Classes on numGuns >= 10 and Ships.class = Classes.class;

--15
select hd
from PC
group by hd
having count(hd) >= 2;

--16
select distinct A.model, B.model, A.speed, A.ram
from PC as A,
     PC as B
where A.model > B.model
  and A.speed = B.speed
  and A.ram = B.ram;

--17
select distinct 'Laptop', model, speed
from Laptop
where speed < (select min(speed) from PC);

--18
select distinct maker, price
from Product pr
         join Printer p
              on pr.model = p.model and color = 'y' and price = (select min(price) from Printer where color = 'y');

--19
select maker, avg(screen)
from Product
         join Laptop on Product.model = Laptop.model
group by maker;

--20
select maker, count(model)
from Product
where type = 'PC'
group by maker
having count(model) >= 3;

--21
select maker, max(price)
from Product p
         join PC pc on p.model = pc.model
group by maker;

--22
select spd, avg(prc)
from (select speed as spd, price as prc from PC where speed > 600) as spds
group by spd;

--23
select distinct maker
from Product
where model in (select model from PC where speed >= 750)
intersect
select distinct maker
from Product
where model in (select model from Laptop where speed >= 750);

--24
with al as (
    select model, max(price) as prc
    from PC
    group by model
    union all
    select model, max(price) as prc
    from Laptop
    group by model
    union all
    select model, max(price) as prc
    from Printer
    group by model
)
select model
from al
where prc = (select max(prc) from al);

--25
with min_ram_max_spd as (select distinct model, ram, speed
                         from PC
                         where ram <= all (select ram from PC)
                           and speed >= all (select speed from PC where ram <= all (select ram from PC)))
select distinct maker
from Product
         join min_ram_max_spd on
            Product.model in (select model from min_ram_max_spd) and
            maker in (select distinct maker from Product where type = 'Printer');

--26
select avg(price)
from (
         select price
         from PC
                  join Product on PC.model = Product.model and maker = 'A'
         union all
         select price
         from Laptop
                  join Product on Laptop.model = Product.model and maker = 'A') as Ls;

--27

select maker, avg(hd)
from Product
         join PC on
            Product.model = PC.model and
            (maker in (
                select distinct maker
                from Product
                where type = 'Printer'
            )
                )
group by maker;

--28
select count(maker)
from (
         select maker, count(model) as mdlCount
         from Product
         group by maker
     ) as Ls
where mdlCount = 1;

--29
--missed

--30
--missed

--31
select class, country
from Classes
where bore >= 16;

--32
--missed

--33
select ship
from Outcomes
where battle = 'North Atlantic'
  and result = 'sunk';

--34
select name
from Ships
         join Classes on
            Ships.class = Classes.class and
            displacement > 35000 and
            launched >= 1922 and type = 'bb';

--35
select model, type
from product
where model not like '%[^0-9]%'
   or model not like '%[^a-z]%';

--36
select name
from Ships
         join Classes on Classes.class = name
union
select ship
from Outcomes
         join Classes on class = ship;

--37
select class
from (select class, count(name)
      from (select Classes.class, name
            from Classes
                     join Ships on Classes.class = Ships.class
            union
            select class, ship
            from Classes
                     join Outcomes on ship = class) ls1
      group by class
      having count(*) = 1
     ) ls2;

--38
select distinct country
from Classes
where type = 'bb'
intersect
select distinct country
from Classes
where type = 'bc';

--39
with ship_battle as (
    select ship, name, date, result
    from Battles
             join Outcomes on name = battle
)
select distinct first.ship
from ship_battle as first
         join ship_battle as second
              on first.result = 'damaged' and
                 first.date < second.date and
                 first.ship = second.ship;

--40
select distinct maker, type
from product
where maker in (
    select maker
    from product
    group by maker
    having count(model) > 1
       and count(distinct type) = 1
);

--41
with prpr as (
    select Product.model, price, maker
    from PC
             join Product on PC.model = Product.model
    union

    select Product.model, price, maker
    from Laptop
             join Product on Laptop.model = Product.model
    union

    select Product.model, price, maker
    from Printer
             join Product on Printer.model = Product.model
)
select maker,
       case when price is null then null else max(price) end prc
from prpr
group by maker;

--42
select ship, battle
from Outcomes
where result = 'sunk';

--43
--Missed

--44
select name from Ships where name like 'R%'
union
select ship as name from Outcomes where ship like 'R%';


